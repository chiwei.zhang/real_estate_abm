import numpy as np
import pandas as pd
from geopy.geocoders import Nominatim


def get_latlon_from_address(address):
    geolocator = Nominatim(user_agent='my_app')
    loc = geolocator.geocode(address)
    return loc.latitude, loc.longitude


def get_bin(df, latbin:int, lonbin:int, timebin:int):
    """
    Returns the subset of the passed DataFrame corresponding to the correct
    gridbox and time period.

    Returns a view.
    """

    mask = df.latbin == latbin
    mask = np.logical_and(mask, df.lonbin == lonbin)
    mask = np.logical_and(mask, df.timebin == timebin)
    return df.loc[mask]

def get_neighbors(df, latbin:int, lonbin:int, timebin:int,
                  size=1, time_history=0):
    """
    Returns the subset of the passed DataFrame corresponding to the correct
    neighborhood and time period.

    Returns a view.
    """
    mask1 = np.logical_and(df.latbin >= latbin - size,
                           df.latbin <= latbin + size)
    mask1 = np.logical_and(mask1, df.latbin != latbin)

    mask2 = np.logical_and(df.lonbin >= lonbin - size,
                           df.lonbin <= lonbin + size)
    mask2 = np.logical_and(mask2, df.lonbin != lonbin)

    mask = np.logical_and(mask1, mask2)

    mask3 = np.logical_and(df.timebin >= timebin - time_history,
                           df.timebin <= timebin)

    mask = np.logical_and(mask, mask3)

    return df.loc[mask]


def get_stats(df_subset):
    stats     = np.zeros(12)

    stats[0]  = np.quantile(df_subset.year_built, 0.25)
    stats[1]  = np.quantile(df_subset.year_built, 0.50)
    stats[2]  = np.quantile(df_subset.year_built, 0.75)

    stats[3]  = np.quantile(df_subset.living_area, 0.25)
    stats[4]  = np.quantile(df_subset.living_area, 0.50)
    stats[5]  = np.quantile(df_subset.living_area, 0.75)

    stats[6]  = np.quantile(df_subset.quality, 0.25)
    stats[7]  = np.quantile(df_subset.quality, 0.50)
    stats[8]  = np.quantile(df_subset.quality, 0.75)

    stats[9]  = np.quantile(df_subset.sale_price, 0.25)
    stats[10] = np.quantile(df_subset.sale_price, 0.50)
    stats[11] = np.quantile(df_subset.sale_price, 0.75)

    return stats
