# real_estate_abm

This repo contains code for modeling housing prices in Howard County, MD as an
agent-based model (ABM). Few assumptions are made towards how relationships
between agents impact future agent states. Rather, the dynamics of inter-agent
state changes are derived from the dataset by training a multilayer perceptron
(MLP) to model said dynamics.


## Dataset

Real estate transactions in the United States are recorded in public record.
The state of Maryland supports a search engine that one can use to access that
data. The results discussed in this repository are based on sales in Howard
County from April 11, 2019 to April 10, 2020.

Unfortunately, there are some caveats I will admit up front. The dataset I used
is incomplete. While I was painstakingly able to save off a year's worth
of data, the search engine does not provide the latitude and longitude of the
property sold. The dataset I used was the subset of the data that I successfully
geocoded.


## An agent-based model w/MLP

Conway's game of life is an example of an ABM. And for those unfamiliar with
Conway's game of life, imagine you had a square petri dish divided into a grid.
Each box in the grid either contains a living cell or does not. You are able
to observe the state of the petri dish at discrete points over time. Having
observed the petri dish at one time point, you are able to determine the state
of the petri dish at the next time point because the next state for each box
is a function of other boxes in the grid. That is Conway's game of life

There are some challenges when applying agent-based modeling to house prices.
Unlike cell cultures in a petri dish, I am unable to perfectly observe every
particular property's value.

The approach must be more creative.

I chose to divide the world into a latitude and longitude grid. One common
objection for such a grid is the fact that a unit change in longitude
corresponds to different physical distances depending on latitude. This is a
product of the Earth being round. However, the size of the area I've analyzed is
small enough such that the curvature of the Earth does not come into play.

In terms of observing the state of the grid over time, I consider the median
house value of each grid box to be the state of interest. I assume the median
sales price for the area in public record during a specific time period to be a
reasonable proxy for the underlying true state. Note that this implicitly
requires me to bin the housing market sales history into time periods.

In terms of being able to observe the update dynamics of the housing market I
propose the following framework. I am able to observe an update for a given
lat-lon grid box if I have its sales history for 2 consecutive time periods.
Furthermore, I am able model how grid boxes impact each other if I have sales
data for the surrounding grid boxes during the earlier time period (and perhaps
even earlier). Finally, only grid boxes that are physically near the box of
interest will significantly impact the update. I will refer to those boxes as
the box of interest's "neighborhood." This corresponds to a common belief that
property values tend to be similar to property values in the nearby area.

These criteria ultimately allowed me to extract 639 observable updates in my
datasets. I chose a grid box size slightly less that 2 miles x 2 miles. I
considered a 5-box by 5-box region centered at (but not including) the grid box
of interest as the neighborhood. The sales history was divided into non-
overlapping time periods that were 2 weeks long. Neighborhood statistics
were derived using sales from both the present time period and the previous time
period. I want to point out that allowing time history in an ABM makes it more
expressive than what you see typically in simpler ABMs like Conway's game of
life because the ABM no longer assumes Markovian dynamics. That is, future
states depend on both present and past states.

My approach is to train an MLP to predict the update given statistics about the
present state of a grid box and its neighborhood. That is, given a set of
features, can I train an MLP to predict the percent change of median
house sale price one time period ahead?



## Feature Extraction and Training

Because this study is still in a rudimentary stage, the features I have selected
are simply statistics of data fields that the Maryland search engine readily
provided. They are listed below.

* Neighborhood 25%, 50%, 75% quantiles for when the dwelling was built
* Neighborhood 25%, 50%, 75% quantiles for dwelling living area (sq ft)
* Neighborhood 25%, 50%, 75% quantiles for dwelling quality (see link below)
* Neighborhood 25%, 50%, 75% quantiles for sold prices in current time period
* Box of interest 25%, 50%, 75% quantiles for when the dwelling was built
* Box of interest 25%, 50%, 75% quantiles for dwelling living area (sq ft)
* Box of interest 25%, 50%, 75% quantiles for dwelling quality (see link below)
* Box of interest 25%, 50%, 75% quantiles for sold prices in current time period

All features are 0-1 normalized after they're extracted. The update ground truth
is calculated as % change in median sale price. This too is 0-1 normalized.

MLPs were trained through 3-fold cross-validation to infer % change in median
sale price from the features listed. Mean absolute error was used as a loss
function.


## Results

Surprisingly all folds performed similarly showing mean absolute error losses of
about 17-19% on both training and validation sets. This seems to imply that
overfitting is not the primary risk with my current model. I would say
improvements will like come with some combination of a deeper MLP, features
that are more descriptive of grid boxes/neighborhoods, or larger grid boxes
and longer time periods observed to better estimate the statistic of the data.


## Possible next steps

As I was aiming for a proof-of-concept, there remains several paths for
advancing the capabilities developed in this repository. Below are my
suggestions. I've limited the list to the 3 I believe are most promising.

* Alternative models or deep learning architectures
* Engineering more nuanced features
* Tuning grid box size, time period duration, neighborhood size, time history
  made visible to the MLP-ABM


## References
MESA Documentation https://mesa.readthedocs.io/en/master/
Pattern Recognition and Machine Learning by Bishop
Deep Learning by Courville, Goodfellow, and Bengio
Agent‐based modeling approach for group polarization behavior considering
conformity and network relationship strength by Zhang, Wang, Chen, Shi
